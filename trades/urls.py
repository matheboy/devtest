from django.conf.urls import url

from . import views

app_name = 'trades'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^new_trade/$', views.new_trade, name='new_trade'),
    url(r'^update_trade/$', views.update_trade, name='update_trade'),
]