# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.http import JsonResponse
from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.template import loader
from django.urls import reverse

from .models import Trade
from .forms import TradeForm

import fixerio



def index(request):
    template = loader.get_template('trades/index.html')
    context = {'trade_list': Trade.objects.all().order_by('-date_booked')}
    return HttpResponse(template.render(context, request))



def new_trade(request):
    if request.method == 'POST':
        form = TradeForm(request.POST, request.FILES)
        if form.is_valid():
            obj = Trade()
            obj.id = obj.new_id()
            obj.sell_currency=form.cleaned_data['sell_currency']
            obj.sell_amount=form.cleaned_data['sell_amount']
            obj.buy_currency=form.cleaned_data['buy_currency']
            obj.buy_amount=form.cleaned_data['buy_amount']
            obj.rate=form.cleaned_data['rate']
            obj.save()
            return HttpResponseRedirect(reverse('trades:index'))
    else:
        form = TradeForm()
    return render(request, 'trades/new_trade.html', {'form': form})


    
def update_trade(request):
    '''
    This will get the data from fixerio to update our UI.
    todo: we need here a loading screen - and timeout -  to block the UI until we get the data.
    '''
    data={'ratio': 1}
    sell_currency = request.GET.get('sell_currency', None)
    buy_currency = request.GET.get('buy_currency', None)
    if sell_currency and buy_currency:
        aux = fixerio.Fixerio()
        rates = aux.latest(base=sell_currency) 
        ratio = rates['rates'].get(buy_currency,1)
        data = {'ratio': ratio}
    return JsonResponse(data)



