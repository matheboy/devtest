# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import uuid


BASE62 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
def encode(num, alphabet=BASE62):
    """Encode a positive number in Base X
    Arguments:
    - `num`: The number to encode
    - `alphabet`: The alphabet to use for encoding
    """
    if num == 0:
        return alphabet[0]
    arr = []
    base = len(alphabet)
    while num:
        num, rem = divmod(num, base)
        arr.append(alphabet[rem])
    arr.reverse()
    return ''.join(arr)


def decode(string, alphabet=BASE62):
    """Decode a Base X encoded string into the number
    Arguments:
    - `string`: The encoded string
    - `alphabet`: The alphabet to use for encoding
    """
    base = len(alphabet)
    strlen = len(string)
    num = 0
    idx = 0
    for char in string:
        power = (strlen - (idx + 1))
        num += alphabet.index(char) * (base ** power)
        idx += 1
    return num




class Trade(models.Model):
    # having a database id like this is bad
    # there are multiple options that would be more easy and eficient to deal with.
    id = models.CharField(max_length=9, primary_key=True)
    sell_currency=models.CharField(max_length=200)
    sell_amount=models.FloatField()
    buy_currency=models.CharField(max_length=200)
    buy_amount=models.FloatField()
    rate=models.FloatField()
    date_booked=models.DateTimeField('date booked', auto_now_add=True)
    
    
    # I would prefer storing in the database a plain integer 
    # that is eventually converted to alphanumeric if needed
    # If the code is used to define entries I may needed here to query
    def new_id(self):
        num = Trade.objects.filter(id__startswith = 'TD').count()
        return 'TD'+encode(num).zfill(7)
    
 